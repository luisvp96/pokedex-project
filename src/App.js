import "./App.css";
import { useState } from "react";
import Axios from "axios";
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import {About} from "./Components/About/About";
import { Location } from "./Components/Location/Location";

const App = () => {
  const [pokemonName, setPokemonName] = useState("");
  const [pokemonChosen, setPokemonChosen] = useState(false);
  const [pokemon, setPokemon] = useState({
    name: "",
    number: "",
    species: "",
    image: "",
    hp: "",
    attack: "",
    defense: "",
    speed: "",
    type: "",
});

  const searchPokemon = () => {
    Axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemonName}`).then(
      (res) => {
        setPokemon({
          name: pokemonName,
          number: res.data.id,
          species: res.data.species.name,
          image: res.data.sprites.front_default,
          hp: res.data.stats[0].base_stat,
          attack: res.data.stats[1].base_stat,
          defense: res.data.stats[2].base_stat,
          speed: res.data.stats[5].base_stat,
          type: res.data.types[0].type.name,
        });
        setPokemonChosen(true);
      }
    ); 
  };
  return (
    <div className="App">
      <div className="TitleSection">
        <h1>Pokédex</h1>
        <input type="text" onChange={(event) => { setPokemonName(event.target.value); }} value={pokemonName.toLowerCase()} />
        <div>
          {pokemonName && <button onClick={searchPokemon}>Search Pokémon</button>}
        </div>
      </div>
      <div className="DisplaySection">
      {!pokemonChosen ? ( 
        <h1> Please choose a Pokémon </h1>
         ):(
        <> 
          <h1>{pokemon.name}</h1>
          <img src={pokemon.image} alt={pokemon.name} />
          <div className="DisplaySectionDivision">
            <div className="DisplaySectionDivisionDetails">
              <h4>Number: #{pokemon.number}</h4>
              <h4>Species: {pokemon.species}</h4>
              <h4>Type: {pokemon.type}</h4>
            </div>
            <div className="DisplaySectionDivisionDetails">
              <h4>Hp: {pokemon.hp}</h4>
              <h4>Attack: {pokemon.attack}</h4>
              <h4>Defense: {pokemon.defense}</h4>
              <h4>Speed: {pokemon.speed}</h4>
            </div>
          </div>
        </> 
        )}
      </div>
      <Router>
      <div>
        <Link to={"/about"} className="Links">
          <button>About</button>
        </Link>
        <Link to={"/location"} className="Links">
          <button>Location</button>
        </Link>
        <Switch>
          <Route path="/about">
            <About />
          </Route>
          <Route path="/Location">
            <Location />
          </Route>
        </Switch>
      </div>
    </Router>
    </div>
    
  )
};
export default App;